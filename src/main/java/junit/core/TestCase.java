package junit.core;

import java.lang.reflect.Method;

/**
 * Created by hotjoyit on 2017-10-09.
 */
public abstract class TestCase implements Test {
  private final String targetMethodName;

  public TestCase(String testMethod) {
    this.targetMethodName = testMethod;
  }

  @Override
  public void run(TestResult testResult) {
    setUp();
    runTest(testResult);
    tearDown();
  }

  private void runTest(TestResult testResult) {
    try {
      testResult.testStarted();
      Method method = this.getClass().getMethod(targetMethodName);
      method.invoke(this);
    } catch (Exception e) {
      testResult.testFailed();
    }
  }

  protected void setUp() {}
  protected void tearDown() {}
}
