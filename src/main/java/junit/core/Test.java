package junit.core;

/**
 * Created by hotjoyit on 2017-10-09.
 */
public interface Test {
  void run(TestResult testResult);
}
