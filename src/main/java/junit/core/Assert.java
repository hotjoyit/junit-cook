package junit.core;

/**
 * Created by hotjoyit on 2017-10-08.
 */
public class Assert {

  public static void assertTrue(boolean shouldTrue) {
    if (!shouldTrue) {
      throw new AssertionError("Expected true but got false");
    }
  }
}
