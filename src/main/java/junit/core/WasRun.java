package junit.core;

/**
 * Created by hotjoyit on 2017-10-08.
 */
public class WasRun extends TestCase {

  private String log;

  public WasRun(String testMethod) {
    super(testMethod);
  }

  public void testMethod() {
    this.log += " testMethod";
  }

  public void failingMethod() {
    throw new RuntimeException();
  }

  @Override
  protected void setUp() {
    this.log = "setUp";
  }

  @Override
  protected void tearDown() {
    this.log += " tearDown";
  }

  public String getLog() {
    return log;
  }
}
