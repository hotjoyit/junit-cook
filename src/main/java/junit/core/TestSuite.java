package junit.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hotjoyit on 2017-10-09.
 */
public class TestSuite implements Test {
  private List<TestCase> suite = new ArrayList<>();

  public void addTestCase(TestCase testCase) {
    suite.add(testCase);
  }

  @Override
  public void run(TestResult testResult) {
    suite.parallelStream().forEach(testCase -> testCase.run(testResult));
  }
}
