package junit.core;

/**
 * Created by hotjoyit on 2017-10-09.
 */
public class TestResult {

  private int runCount = 0;
  private int failedCount = 0;

  public synchronized void testStarted() {
    this.runCount++;
  }

  public synchronized void testFailed() {
    this.failedCount++;
  }

  public String report() {
    return String.format("%d run, %d failed", runCount, failedCount);
  }
}
