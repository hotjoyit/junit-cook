import junit.core.Assert;
import junit.core.TestCase;
import junit.core.TestResult;
import junit.core.TestSuite;

/**
 * Created by hotjoyit on 2017-10-09.
 */
public class JUnitTest extends TestCase {

  public JUnitTest(String testMethod) {
    super(testMethod);
  }

  @Override
  public void setUp() {
    System.out.println("SETUP");
  }

  @Override
  public void tearDown() {
    System.out.println("TEAR DOWN");
  }

  public void 안녕하세요_TDD_테스트() {
    HelloJUnit hj = new HelloJUnit();
    Assert.assertTrue("Hello TDD".equals(hj.hello()));
  }

  public static void main(String[] args) {
    TestSuite testSuite = new TestSuite();
    testSuite.addTestCase(new JUnitTest("안녕하세요_TDD_테스트"));
    testSuite.addTestCase(new JUnitTest("안녕하세요_TDD_테스트"));
    TestResult testResult = new TestResult();
    testSuite.run(testResult);
    System.out.println(testResult.report());
  }

  private class HelloJUnit {
    public String hello() {
      return "Hello TDD";
    }
  }
}
