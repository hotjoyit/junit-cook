import junit.core.*;

/**
 * Created by hotjoyit on 2017-10-08.
 */
public class TestCaseTest extends TestCase {

  private WasRun test;
  private WasRun failingTest;
  private TestResult result;

  public TestCaseTest(String testMethod) {
    super(testMethod);
  }

  @Override
  protected void setUp() {
    this.test = new WasRun("testMethod");
    this.failingTest = new WasRun("failingMethod");
    this.result = new TestResult();
  }

  public void testTestFlow() {
    test.run(result);
    Assert.assertTrue("setUp testMethod tearDown".equals(test.getLog()));
  }

  public void testSuccessReportFormatting() {
    test.run(this.result);
    Assert.assertTrue("1 run, 0 failed".equals(result.report()));
  }

  public void testFailedReportFormatting() {
    failingTest.run(result);
    Assert.assertTrue("1 run, 1 failed".equals(result.report()));
  }

  public void testTearDownInFailingTest() {
    failingTest.run(result);
    Assert.assertTrue(failingTest.getLog().contains("tearDown"));
  }

  public void testSuiteTest() {
    TestSuite testSuite = new TestSuite();
    testSuite.addTestCase(new TestCaseTest("testTestFlow"));
    testSuite.addTestCase(new TestCaseTest("testSuccessReportFormatting"));
    testSuite.run(result);
    Assert.assertTrue("2 run, 0 failed".equals(result.report()));
  }

  public static void main(String[] args) {
    TestSuite testSuite = new TestSuite();
    testSuite.addTestCase(new TestCaseTest("testTestFlow"));
    testSuite.addTestCase(new TestCaseTest("testSuccessReportFormatting"));
    testSuite.addTestCase(new TestCaseTest("testFailedReportFormatting"));
    testSuite.addTestCase(new TestCaseTest("testTearDownInFailingTest"));
    testSuite.addTestCase(new TestCaseTest("testSuiteTest"));
    TestResult result = new TestResult();
    testSuite.run(result);
    System.out.println(result.report());
  }
}
